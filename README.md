# RFSVGCache

[![CI Status](https://img.shields.io/travis/jeka-mel/RFSVGCache.svg?style=flat)](https://travis-ci.org/jeka-mel/RFSVGCache)
[![Version](https://img.shields.io/cocoapods/v/RFSVGCache.svg?style=flat)](https://cocoapods.org/pods/RFSVGCache)
[![License](https://img.shields.io/cocoapods/l/RFSVGCache.svg?style=flat)](https://cocoapods.org/pods/RFSVGCache)
[![Platform](https://img.shields.io/cocoapods/p/RFSVGCache.svg?style=flat)](https://cocoapods.org/pods/RFSVGCache)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RFSVGCache is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RFSVGCache'
```

## Author

jeka-mel, iosdeveloper.mail@gmail.com

## License

RFSVGCache is available under the MIT license. See the LICENSE file for more info.
