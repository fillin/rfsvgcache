// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.
import PackageDescription

let package = Package(
    name: "RFSVGCache",
    platforms: [
      .iOS(.v9)
    ],
    products: [        
        .library(
            name: "RFSVGCache",
            targets: ["RFSVGCache"]
        )
    ],
    dependencies: [
        .package(name: "PocketSVG", url: "https://github.com/pocketsvg/PocketSVG", from: "2.6.0")
    ],
    targets: [     
        .target(
            name: "RFSVGCache",
            dependencies: ["PocketSVG"],
            path: "RFSVGCache"
        )
    ]
)
