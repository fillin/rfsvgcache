//
//  UIKitExtensions.swift
//  RFSVG
//
//  Created by Dunja Lalic on 1/30/17.
//  Copyright © 2017 Lautsprecher Teufel GmbH. All rights reserved.
//

import UIKit

public extension UIButton {
    
    /// Creates an image from SVG file and sets it to the button
    /// where image size is determined from the button bounds and `imageEdgeInsets`,
    ///
    /// - Important:
    ///   - The image rendered is rendered in `alwaysTemplate` mode
    ///   - Button's imageView content mode is set to `scaleAspectFit`
    ///
    /// - Parameters:
    ///   - name: Name of SVG file
    ///   - state: The state that uses the specified title. The values are described in `UIControlState`
    func setImageFromSVG(_ name: String, for state: UIControl.State) {
        var rect = self.bounds.inset(by: self.titleEdgeInsets)
        rect = rect.inset(by: self.imageEdgeInsets)
        var image = UIImage.imageFromSVG(name, size: rect.size, color: tintColor)
        if image.cgImage != nil {
            image = image.withRenderingMode(.alwaysTemplate)
        }
        self.imageView?.contentMode = .scaleAspectFit
        self.setImage(image, for: state)
    }
}

public extension UIImageView {
    
    /// Creates an image from SVG file and sets it to the image view
    /// where image size is determined from the image view bounds.
    ///
    /// - Important:
    ///   - Button's imageView content mode is set to `scaleAspectFit`
    ///
    /// - Parameters:
    ///   - name: Name of SVG file
    func setImageFromSVG(_ name: String, color: UIColor? = nil) {
        self.contentMode = .scaleAspectFit
        let image = UIImage.imageFromSVG(name, size: self.bounds.size, color: color)
        self.image = image
    }
}

public extension UIImage {
    
    /// Creates an image from SVG file and returns it.
    ///
    /// - Parameters:
    ///   - name: Name of SVG file
    ///   - size: Desired size
    /// - Returns: A freshly rendered or cached (if existing version for size exists) `UIImage` from SVG file
    static func imageFromSVG(_ name: String, size: CGSize, color: UIColor? = nil) -> UIImage {
        RFSVGCache.sharedInstance.image(name: name, size: size, color: color)
    }
    
    static func from(svg name: String, size: CGSize, color: UIColor? = nil) -> UIImage {
        self.imageFromSVG(name, size: size, color: color)
    }
    
    convenience init?(svg fileName: String, size: CGSize, color: UIColor? = nil) {
        let img = UIImage.imageFromSVG(fileName, size: size, color: color)
        guard let pic = img.cgImage else {
            return nil
        }
        self.init(cgImage: pic, scale: img.scale, orientation: img.imageOrientation)
    }
}
