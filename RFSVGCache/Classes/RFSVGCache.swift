//
//  RFSVGCache.swift
//  RFSVG
//
//  Created by Dunja Lalic on 1/25/17.
//  Copyright © 2017 Lautsprecher Teufel GmbH. All rights reserved.
//

import Foundation
import UIKit
import PocketSVG

open class RFSVGCache: DirectoryWatcherDelegate {
    // MARK: Properties
    
    /// Singleton
    public static let sharedInstance = RFSVGCache()
    
    /// A bundle to determine where to load SVG files from, defaults to main bundle.
    public var bundle: Bundle?
    
    /// Indicates if each image borders should be highlighted. Used for debug purposes.
    public var debugBorders = false
    
    /// Used for monitoring files saved to disk
    private var directoryWatcher: DirectoryWatcher?
    
    /// Used for caching images in-memory
    private let imageCache: NSCache<NSString, UIImage> = NSCache()
    
    /// Used for writing images to the disk
    private var writeQueue: DispatchQueue = DispatchQueue(label: "com.raumfeld.SerialSVGCacheQueue", attributes: [])
    
    private let cleanDiskCacheOnStart = false
    
    private var defaultFileSuffix: String {
        return "_@" + String(describing: Int(UIScreen.main.scale)) + "x"
    }
    
    private var defaultFileExtension: String { return ".png" }
    
    var cacheDirName: String { return "com.raumfeld.SVGCache" }
    
    // MARK: Lifecycle
    
    /// Initializer that subscribes to `UIApplicationDidBecomeActive` and `UIApplicationDidEnterBackground`
    /// to know when to start monitoring a folder. Also subscribes to `UIApplicationDidReceiveMemoryWarning`
    /// to know when to clear in-memory image cache if it's not automatically done by `NSCache`.
    private init() {
        if self.bundle == nil {
            self.bundle = Bundle.main
        }
        
        if cleanDiskCacheOnStart {
            self.cleanCaches()
        }
        
        do {
            try FileManager.default.createDirectory(atPath: pathForTemporaryDirectory, withIntermediateDirectories: false, attributes: nil)
        } catch let error {
            if (error as NSError).code != NSFileWriteFileExistsError {
                debugPrint("Error creating folder at path \(pathForTemporaryDirectory)")
            }
        }
        
        self.imageCache.name = self.cacheDirName
        
        self.directoryWatcher = DirectoryWatcher.init(URL: pathURLForTemporaryDirectory(), delegate: self)
        
        let didBecomeActiveBlock = { [unowned self] (_: Notification) in
            self.handleApplicationDidBecomeActive()
        }
        
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: didBecomeActiveBlock)
        
        let didEnterBackgroundBlock = { [unowned self] (_: Notification) in
            self.handleApplicationDidEnterBackgroundBlock()
        }
        
        NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: didEnterBackgroundBlock)
        
        let removalBlock = { [unowned self] (_: Notification) in
            self.handleLowMemoryWarning()
        }
        
        NotificationCenter.default.addObserver(forName: UIApplication.didReceiveMemoryWarningNotification,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: removalBlock)
        debugPrint("\(type(of: self)) initialized")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didReceiveMemoryWarningNotification, object: nil)
        self.directoryWatcher?.stopMonitoring()
    }
    
    /// Creates an image from SVG file and returns it.
    ///
    /// - Parameters:
    ///   - name: Name of SVG file
    ///   - size: Desired size
    /// - Returns: A freshly rendered or cached (if existing version for size exists) `UIImage` from SVG file
    public func image(name: String, size: CGSize, color: UIColor? = nil) -> UIImage {
        if let image = imageFromMemoryCache(name: name, size: size, color: color) {
            return image
        }
        if imageExistsInDiskCache(name: name, size: size, color: color) {
            return imageFromDiskCache(name: name, size: size, color: color)
        }
        let image = imageFromSVG(name: name, size: size, fillColor: color)
        cacheImageAsync(image: image, name: name, size: size, color: color)
        return image
    }
    
    // MARK: File management
    
    /// Creates a image name combined from name and size.
    ///
    /// - Parameters:
    ///   - name: Name of SVG file
    ///   - size: Desired size
    /// - Returns: A string as a combination of name and size
    private func imageName(name: String, size: CGSize, fillColor: UIColor? = nil) -> String {
        var path = "\(name)_\(size.width)x\(size.height)"
        if fillColor != nil, let components = fillColor?.cgColor.components {
            var colorAsString = ""
            components.forEach({ (item) in
                colorAsString += String(format: "%f.3", item)
            })
            path += "_\(colorAsString)"
        }
        if UIScreen.main.scale > 1 {
            path += self.defaultFileSuffix
        }
        path += self.defaultFileExtension
        return path
    }
    
    /// Path for a directory to store image files in.
    ///
    /// - Returns: A path inside caches directory
    var pathForTemporaryDirectory: String {
        var path = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first ?? ""
        path += "/"
        path += self.cacheDirName
        return path
    }
    
    /// A URL based on `pathForTemporaryDirectory`.
    ///
    /// - Returns: An URL of the path inside caches directory
    private func pathURLForTemporaryDirectory() -> URL {
        return URL(fileURLWithPath: pathForTemporaryDirectory)
    }
    
    /// Image name for image stored in disk cache.
    ///
    /// - Parameters:
    ///   - name: Name of SVG file
    ///   - size: Desired size
    /// - Returns: A path inside caches directory appended by image name
    private func pathForImage(name: String, size: CGSize, color: UIColor? = nil) -> String {
        let fileName = self.imageName(name: name, size: size, fillColor: color)
        return pathForTemporaryDirectory + "/" + fileName
    }
    
    /// A URL based on `pathForImage`.
    ///
    /// - Parameters:
    ///   - name: Name of SVG file
    ///   - size: Desired size
    /// - Returns: An URL inside caches directory appended by image name
    private func pathURLForImage(name: String, size: CGSize, color: UIColor? = nil) -> URL {
        return URL(fileURLWithPath: pathForImage(name: name, size: size, color: color))
    }
    
    // MARK: SVG parsing
    
    /// Creates a `SVGLayer` and takes a snapshot of it.
    ///
    /// - Note:
    ///   - sets the layer gravity to `kCAGravityResizeAspect`
    ///   - scales the lines
    ///
    /// - Parameters:
    ///   - name: Name of SVG file
    ///   - size: Desired size
    /// - Returns: An snapshot of `SVGLayer`
    private func imageFromSVG(name: String, size: CGSize, fillColor: UIColor? = nil) -> UIImage {
        guard let url = self.bundle?.url(forResource: name, withExtension: "svg") else {
            if _isDebugAssertConfiguration() {
                assertionFailure("Image '\(name).svg' not found!")
            }
            return UIImage()
        }
        let layer: SVGLayer = SVGLayer(contentsOf: url)
        layer.contentsGravity = CALayerContentsGravity.resizeAspect
        layer.frame = CGRect(origin: CGPoint.zero, size: size)
        layer.scaleLineWidth = true
        if fillColor != nil {
            layer.fillColor = fillColor!.cgColor
        }
        if debugBorders {
            layer.borderColor = fillColor?.cgColor ?? UIColor.blue.cgColor
            layer.borderWidth = 0.5
        }
        let image = imageForLayer(layer: layer)
        return image
    }
    
    /// Creates a snapshot of a layer.
    ///
    /// - Parameter layer: A layer
    /// - Returns: An image, empty if no image bounds are set
    private func imageForLayer(layer: CALayer) -> UIImage {
        let bounds = layer.frame
        if bounds.size.width == 0 || bounds.size.height == 0 {
            return UIImage()
        }
        UIGraphicsBeginImageContextWithOptions(bounds.size, layer.isOpaque, UIScreen.main.scale)
        defer { UIGraphicsEndImageContext() }
        guard let context: CGContext = UIGraphicsGetCurrentContext() else {
            return UIImage()
        }
        context.saveGState()
        layer.layoutIfNeeded()
        layer.render(in: context)
        context.restoreGState()
        return UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
    }
    
    // MARK: Caching
    
    /// Retrieves an image from in-memory cache.
    ///
    /// - Parameters:
    ///   - name: Name of SVG file
    ///   - size: Desired size
    /// - Returns: An image loaded from the in-memory cache.
    func imageFromMemoryCache(name: String, size: CGSize, color: UIColor? = nil) -> UIImage? {
        return self.imageCache.object(forKey: imageName(name: name, size: size, fillColor: color) as NSString)
    }
    
    /// Checks if image exists in disk cache.
    ///
    /// - Parameters:
    ///   - name: Name of SVG file
    ///   - size: Desired size
    /// - Returns: A boolean value
    func imageExistsInDiskCache(name: String, size: CGSize, color: UIColor? = nil) -> Bool {
        return FileManager.default.fileExists(atPath: pathForImage(name: name, size: size, color: color))
    }
    
    /// Retrieves an image from disk cache.
    ///
    /// - Parameters:
    ///   - name: Name of SVG file
    ///   - size: Desired size
    /// - Returns: An image loaded from the disk.
    func imageFromDiskCache(name: String, size: CGSize, color: UIColor? = nil) -> UIImage {
        do {
            let path = pathURLForImage(name: name, size: size, color: color)
            let pngData: Data = try Data(contentsOf: path)
            let scale: CGFloat = UIScreen.main.scale
            if let image = UIImage(data: pngData, scale: scale) {
                return image
            }
        } catch {
            debugPrint("Error reading file at URL \(pathURLForImage(name: name, size: size, color: color))")
        }
        
        return UIImage()
    }
    
    /// Writes an image to disk.
    ///
    /// - Parameters:
    ///   - image: An image
    ///   - name: Name of SVG file
    ///   - size: Desired size
    private func cacheImageAsync(image: UIImage, name: String, size: CGSize, color: UIColor? = nil) {
        var cost = 0
        if let imageRef = image.cgImage {
            cost = imageRef.bytesPerRow
        } else {
            cost = Int(image.size.height * image.size.width * image.scale * image.scale)
        }
        self.imageCache.setObject(image, forKey: imageName(name: name, size: size, fillColor: color) as NSString, cost: cost)
        
        writeQueue.async { [weak self] in
            guard let weakSelf = self else {
                return
            }
            if let pngData = image.pngData() {
                let fileURL = weakSelf.pathURLForImage(name: name, size: size, color: color)
                do {
                    try pngData.write(to: fileURL, options: .atomic)
                } catch {
                    debugPrint("Error writing file to URL \(fileURL)")
                }
            }
        }
    }
    
    open func cleanCaches() {
        guard FileManager.default.fileExists(atPath: pathForTemporaryDirectory) else {
            return
        }
        do {
            let dirUrl = URL(fileURLWithPath: pathForTemporaryDirectory, isDirectory: true)
            let enumerator = FileManager.default.enumerator(atPath: pathForTemporaryDirectory)
            while let file = enumerator?.nextObject() as? String {
                try FileManager.default.removeItem(at: dirUrl.appendingPathComponent(file))
            }
        } catch let error {
            debugPrint("Error removing cached svg file \(error)")
        }
    }
    
    // MARK: Notifications
    
    private func handleApplicationDidBecomeActive() {
        self.directoryWatcher?.startMonitoring()
    }
    
    private func handleApplicationDidEnterBackgroundBlock() {
        self.directoryWatcher?.stopMonitoring()
    }
    
    private func handleLowMemoryWarning() {
        self.imageCache.removeAllObjects()
    }
    
    // MARK: DirectoryWatcherDelegate
    
    internal func directoryDidChange(_ directoryWatcher: DirectoryWatcher) {
        do {
            let items = try FileManager.default.contentsOfDirectory(atPath: pathForTemporaryDirectory)
            for item: String in items {
                self.imageCache.removeObject(forKey: item as NSString)
            }
        } catch {
            debugPrint("Error reading contents of folder at path \(pathForTemporaryDirectory)")
        }
    }
}
